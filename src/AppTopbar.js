import React, { useContext, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import classNames from "classnames";
import { OverlayPanel } from "primereact/overlaypanel";

/*
Store
*/
//import { AuthContext } from "../data/AuthContext";
import { AuthContext } from "../src/data/AuthContext";

export const AppTopbar = (props) => {
    /*
    Variables 
     */
    const op = useRef(null);

    useEffect(() => {}, [context]);

    /*
    Context  
    */
    const context = useContext(AuthContext);

    return (
        <div className="layout-topbar">
            <Link to="/" className="layout-topbar-logo">
                <img src={props.layoutColorMode === "light" ? "assets/layout/images/logo-dark.png" : "assets/layout/images/logo-white.png"} alt="logo" />
                <span></span>
            </Link>

            <button type="button" className="p-link  layout-menu-button layout-topbar-button" onClick={props.onToggleMenuClick}>
                <i className="pi pi-bars" />
            </button>

            <button type="button" className="p-link layout-topbar-menu-button layout-topbar-button" onClick={props.onMobileTopbarMenuClick}>
                <i className="pi pi-ellipsis-v" />
            </button>

            <ul className={classNames("layout-topbar-menu lg:flex origin-top", { "layout-topbar-menu-mobile-active": props.mobileTopbarMenuActive })}>
                <li>
                    <button className="p-link layout-topbar-button" onClick={props.onMobileSubTopbarMenuClick}>
                        <i className="pi pi-calendar" />
                        <span>Events</span>
                    </button>
                </li>
                <li>
                    <button className="p-link layout-topbar-button" onClick={props.onMobileSubTopbarMenuClick}>
                        <i className="pi pi-cog" />
                        <span>Settings</span>
                    </button>
                </li>
                <li>
                    <button
                        className="p-link layout-topbar-button"
                        onClick={(e) => {
                            op.current.toggle(e);
                        }}
                    >
                        <i className="pi pi-user" />
                        <span title="Logout">Profile</span>
                        <OverlayPanel ref={op} showCloseIcon id="overlay_panel" style={{ width: "150px" }} className="overlaypanel-demo">
                            <div onClick={() => context.logout()}>
                                Logout
                                <i className="pi pi-power-off" />
                            </div>
                        </OverlayPanel>
                    </button>
                </li>
            </ul>
        </div>
    );
};
