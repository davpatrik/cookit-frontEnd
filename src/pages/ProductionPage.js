import React, { useEffect, useRef, useContext } from "react";

/*
Store
*/
import { AuthContext } from "../data/AuthContext";

/*
Services
*/
import ItemsSevice from "../service/ItemsSevice";
import { ItemsList } from "../components/ItemsList";

/*
Utils
*/
import { label } from "../util/Internationalization";

export const ProductionPage = (props) => {
    /*
    Context  
    */
    const context = useContext(AuthContext);

    /*
    Init
    */
    useEffect(() => {
        loadItemList();
    }, []);

    const loadItemList = () => {
        ItemsSevice.queryLstItems().then((response) => {
            context.setLstItems(response);
        });
    };

    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">{!context.lstItems || context.lstItems.length === 0 ? label[context.selLanguage]["loading"] : <ItemsList lstItems={context.lstItems} />}</div>
            </div>
        </div>
    );
};
