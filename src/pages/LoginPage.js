import React, { useEffect, useState, useContext } from "react";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";

/*
Own Components
*/
//import { LoginService } from "../service/LoginService";
import LoginService from "../service/LoginService";
//import logoEnterprise from "/assets/layout/images/logo-white.png";
import { InputFloatLabel } from "../components/InputFloatLabel";
import { PasswordFloatLabel } from "../components/PasswordFloatLabel";

// Import Utils
import { label } from "../util/Internationalization";

/*
Store
*/
import { AuthContext } from "../data/AuthContext";

export const LoginPage = (props) => {
    /*
    Variables
    */
    const [dialogVisible, setDialogVisible] = useState(true);
    const [user, setUser] = useState("user");
    const [password, setPassword] = useState("pass");

    /*
    Context  
    */
    const context = useContext(AuthContext);

    const handleLogin = () => {
        let payload = {
            user,
            password,
        };
        LoginService.login(payload).then((valid) => {
            if (!valid || !valid.token) {
                context.showMessage(valid);
            } else {
                //window.$loggedIn = true;
                context.setAuthUserWithApiKey(valid);
                let msg = {
                    summary: label[context.selLanguage]["login_welcome_message"] + " " + valid.user, //
                    detail: label[context.selLanguage]["welcomeMessage"],
                };
                context.showMessage(msg);
                //setState({ dialogVisible: false });
                setDialogVisible(false);
                props.history.push("/dashboard");
            }
        });
    };
    const dialogFooter = <Button label={label[context.selLanguage]["login_header"]} icon="pi pi-user" onClick={() => handleLogin()} />;

    /**
     * Return
     */
    return (
        <Dialog header={label[context.selLanguage]["login_header"]} visible={dialogVisible} footer={dialogFooter} onHide={() => setDialogVisible(false)} closable={false} draggable={false} modal>
            <div className="p-grid p-fluid" style={{ alignItems: "center", justifyContent: "center" }}>
                <img src={props.layoutColorMode === "light" ? "assets/layout/images/logo-dark.png" : "assets/layout/images/logo-white.png"} alt="logo" />

                <div className="p-col-12 p-lg-12">
                    <div className="p-grid" style={{ padding: 40 }}>
                        <InputFloatLabel value={user} width="4" icon="pi pi-user" label={label[context.selLanguage]["login_user"]} log={""} onChange={(e) => setUser(e)} />
                        &nbsp;
                        <PasswordFloatLabel value={password ? password : ""} log={""} width="4" feedback={false} icon="pi pi-key" label={label[context.selLanguage]["login_password"]} onChange={(e) => setPassword(e)} />
                    </div>
                </div>
            </div>
        </Dialog>
    );
};
