import React, { useState, useEffect, useRef } from "react";
import classNames from "classnames";
import { Route, useHistory } from "react-router-dom";
import { CSSTransition } from "react-transition-group";

import { AppTopbar } from "./AppTopbar";
import { AppFooter } from "./AppFooter";
import { AppMenu } from "./AppMenu";
import { AppConfig } from "./AppConfig";

import { Dashboard } from "./pages/Dashboard";
import { LoginPage } from "./pages/LoginPage";
import { ProductionPage } from "./pages/ProductionPage";
import { EmptyPage } from "./pages/EmptyPage";

import PrimeReact from "primereact/api";
import { Toast } from "primereact/toast";

// Import Utils
import { label } from "./util/Internationalization";

// Style files
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import "prismjs/themes/prism-coy.css";
import "./layout/flags/flags.css";
import "./layout/layout.scss";
import "./App.scss";

/*
Store
*/
import { AuthContext } from "../src/data/AuthContext";

const App = () => {
    /*
    Variables
    */
    const [selLanguage, setSelLanguage] = useState(null);
    const [authUser, setAuthUser] = useState(null);
    const toast = useRef();
    const [lstItems, setLstItems] = useState([]);

    /*
    Layout vars
    */
    const [layoutMode, setLayoutMode] = useState("static");
    const [layoutColorMode, setLayoutColorMode] = useState("light");
    const [inputStyle, setInputStyle] = useState("outlined");
    const [ripple, setRipple] = useState(true);
    const [staticMenuInactive, setStaticMenuInactive] = useState(false);
    const [overlayMenuActive, setOverlayMenuActive] = useState(false);
    const [mobileMenuActive, setMobileMenuActive] = useState(false);
    const [mobileTopbarMenuActive, setMobileTopbarMenuActive] = useState(false);

    //window.$apiKey = "apiKeyDefaultValue";
    //window.$loggedIn = false;

    const history = useHistory();

    PrimeReact.ripple = true;

    let menuClick = false;
    let mobileTopbarMenuClick = false;

    useEffect(() => {
        isLanguageSelected();
        if (mobileMenuActive) {
            addClass(document.body, "body-overflow-hidden");
        } else {
            removeClass(document.body, "body-overflow-hidden");
        }
    }, [mobileMenuActive, selLanguage, authUser]);

    function isLanguageSelected() {
        if (!selLanguage) {
            history.push("/");
        } else {
            isAuthenticated();
        }
    }

    function isAuthenticated() {
        if (!authUser) {
            history.push("/login");
        }
    }

    const onWrapperClick = (event) => {
        if (!menuClick) {
            setOverlayMenuActive(false);
            setMobileMenuActive(false);
        }

        if (!mobileTopbarMenuClick) {
            setMobileTopbarMenuActive(false);
        }

        mobileTopbarMenuClick = false;
        menuClick = false;
    };

    const onToggleMenuClick = (event) => {
        menuClick = true;

        if (isDesktop()) {
            if (layoutMode === "overlay") {
                if (mobileMenuActive === true) {
                    setOverlayMenuActive(true);
                }

                setOverlayMenuActive((prevState) => !prevState);
                setMobileMenuActive(false);
            } else if (layoutMode === "static") {
                setStaticMenuInactive((prevState) => !prevState);
            }
        } else {
            setMobileMenuActive((prevState) => !prevState);
        }

        event.preventDefault();
    };

    const onSidebarClick = () => {
        menuClick = true;
    };

    const onMobileTopbarMenuClick = (event) => {
        mobileTopbarMenuClick = true;

        setMobileTopbarMenuActive((prevState) => !prevState);
        event.preventDefault();
    };

    const onMobileSubTopbarMenuClick = (event) => {
        mobileTopbarMenuClick = true;

        event.preventDefault();
    };

    const onMenuItemClick = (event) => {
        if (!event.item.items) {
            setOverlayMenuActive(false);
            setMobileMenuActive(false);
        }
    };
    const isDesktop = () => {
        return window.innerWidth >= 992;
    };

    const menu = [
        {
            label: label[selLanguage]["menu_pages"],
            icon: "pi pi-fw pi-clone",
            items: [{ label: label[selLanguage]["menu_producion"], icon: "pi pi-fw pi-th-large", to: "/productionPage" }],
        },
        {
            label: label[selLanguage]["menu_getStarted"],
            items: [
                {
                    label: label[selLanguage]["menu_documentation"],
                    icon: "pi pi-fw pi-question",
                    command: () => {
                        window.location = "https://gitlab.com/davpatrik/cookit-frontEnd/-/blob/master/README.md";
                    },
                },
                {
                    label: label[selLanguage]["menu_viewSource"],
                    icon: "pi pi-fw pi-search",
                    command: () => {
                        window.location = "https://gitlab.com/davpatrik/cookit-frontEnd.git";
                    },
                },
            ],
        },
    ];

    const addClass = (element, className) => {
        if (element.classList) element.classList.add(className);
        else element.className += " " + className;
    };

    const removeClass = (element, className) => {
        if (element.classList) element.classList.remove(className);
        else element.className = element.className.replace(new RegExp("(^|\\b)" + className.split(" ").join("|") + "(\\b|$)", "gi"), " ");
    };

    const wrapperClass = classNames("layout-wrapper", {
        "layout-overlay": layoutMode === "overlay",
        "layout-static": layoutMode === "static",
        "layout-static-sidebar-inactive": staticMenuInactive && layoutMode === "static",
        "layout-overlay-sidebar-active": overlayMenuActive && layoutMode === "overlay",
        "layout-mobile-sidebar-active": mobileMenuActive,
        "p-input-filled": inputStyle === "filled",
        "p-ripple-disabled": ripple === false,
        "layout-theme-light": layoutColorMode === "light",
    });

    const logout = () => {
        console.log("logout");
        setAuthUserWithApiKey(null);
    };

    function setAuthUserWithApiKey(obj) {
        setAuthUser(obj);
        window.$apiKey = obj ? obj.token : "";
        //window.$loggedIn = true;
        if (!obj) {
            setSelLanguage(null);
            history.push("/");
        } else {
            history.push("/dashboard");
        }
    }

    /*
    Context Api methods
    */
    const showMessage = (obj) => {
        let msg = "";
        let life = 2500;
        let maxLife = 15000;
        let currentLife = life;
        if (obj.status && obj.error && obj.message && obj.error === "Forbidden" && (obj.message.includes("JWT expired") || obj.message.includes("Access Denied"))) {
            logout();
            //history.push("/");
        } else if (obj.log || obj.success) {
            //if (obj.success) {
            currentLife = obj.log.length * 200 < life ? life : obj.log.length * 100;
            msg = {
                //severity: obj.success ? "success" : "error",
                severity: obj.success ? "success" : obj.severity ? obj.severity : "error",
                summary: "Aviso",
                detail: obj.log,
                sticky: false,
                life: currentLife > maxLife ? maxLife : currentLife,
            };
        } else {
            let summary = obj.summary ? obj.summary : "Error";
            let detail = obj.detail ? obj.detail : obj.toString();
            currentLife = detail.length * 200 < life ? life : detail.length * 100;
            msg = {
                severity: obj.severity ? obj.severity : "success",
                summary: summary,
                detail: detail,
                sticky: false,
                life: currentLife > maxLife ? maxLife : currentLife,
            };
        }
        toast.current ? toast.current.show(msg) : console.log("toast null");
    };

    return (
        <AuthContext.Provider
            value={{
                selLanguage: selLanguage,
                setSelLanguage: setSelLanguage,
                setAuthUserWithApiKey: setAuthUserWithApiKey,
                logout: logout,
                setLstItems: setLstItems,
                lstItems: lstItems,
                showMessage: showMessage,
            }}
        >
            <div className={wrapperClass} onClick={onWrapperClick}>
                <Toast ref={toast} style={{ marginTop: "35px" }} />
                <AppTopbar onToggleMenuClick={onToggleMenuClick} layoutColorMode={layoutColorMode} mobileTopbarMenuActive={mobileTopbarMenuActive} onMobileTopbarMenuClick={onMobileTopbarMenuClick} onMobileSubTopbarMenuClick={onMobileSubTopbarMenuClick} />

                <div className="layout-sidebar" onClick={onSidebarClick}>
                    {selLanguage ? <AppMenu model={menu} onMenuItemClick={onMenuItemClick} layoutColorMode={layoutColorMode} /> : "Sélectionnez une langue / Select a language"}
                </div>

                <div className="layout-main-container">
                    <div className="layout-main">
                        <Route path="/" exact component={Dashboard} />
                        <Route path="/login" exact component={LoginPage} />
                        <Route path="/productionPage" component={ProductionPage} />
                        <Route path="/empty" component={EmptyPage} />
                    </div>

                    <AppFooter layoutColorMode={layoutColorMode} />
                </div>

                <CSSTransition classNames="layout-mask" timeout={{ enter: 200, exit: 200 }} in={mobileMenuActive} unmountOnExit>
                    <div className="layout-mask p-component-overlay"></div>
                </CSSTransition>
            </div>
        </AuthContext.Provider>
    );
};

export default App;
