import { GeneralService } from "./GeneralService";

const servicePort = "8080";
const serviceEndPoint = "/user";

export class LoginService {
    async login(payload) {
        return new GeneralService().asyncPostService(payload, servicePort, serviceEndPoint + "/login");
    }
}

export default new LoginService();
