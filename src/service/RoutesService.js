import { GeneralService } from "./GeneralService";

const servicePort = "8081";
const serviceEndPoint = "/routes";

class RoutesSevice {
    async queryRoutesByItems(payload) {
        return new GeneralService().asyncPostService(payload, servicePort, serviceEndPoint + "");
    }
}

export default new RoutesSevice();
