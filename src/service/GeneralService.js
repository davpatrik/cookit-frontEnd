/**
 * Own components
 */
import { endPoints } from "../config/EndPoints";

export class GeneralService {
    async asyncGetService(url) {
        const endPointCompleteUrl = endPoints.pokeApi.apiRestUrl + "/" + endPoints.pokeApi.contextPath + "/" + url;
        //console.log("endPointCompleteUrl");
        //console.log(endPointCompleteUrl);
        try {
            const requestOptions = {
                method: "GET",
                headers: {
                    "Content-Type": endPoints.pokeApi.contentType,
                    Host: "Postman",
                },
            };

            let response = await fetch(endPointCompleteUrl, requestOptions);
            let json = await response.json();
            return json;
        } catch (error) {
            console.error(error);
            return error;
        }
    }

    async asyncGetServiceByUrl(endPointCompleteUrl) {
        //const endPointCompleteUrl = endPoints.pokeApi.apiRestUrl + "/" + endPoints.pokeApi.contextPath + "/" + url;
        //console.log("endPointCompleteUrl");
        //console.log(endPointCompleteUrl);
        try {
            const requestOptions = {
                method: "GET",
                headers: {
                    "Content-Type": endPoints.pokeApi.contentType,
                    Host: "Postman",
                },
            };

            let response = await fetch(endPointCompleteUrl, requestOptions);
            let json = await response.json();
            return json;
        } catch (error) {
            console.error(error);
            return error;
        }
    }

    async asyncPostService(payload, port, url) {
        const endPointCompleteUrl = endPoints.pokeApi.apiRestUrl + ":" + port + url;
        try {
            const requestOptions = {
                method: "POST",
                headers: {
                    "Content-Type": endPoints.pokeApi.contentType,
                    "API-KEY": endPoints.pokeApi.apiKey,
                    "JWT-KEY": window.$apiKey,
                },
                body: JSON.stringify(payload),
            };
            let response = await fetch(endPointCompleteUrl, requestOptions);
            let json = null;
            if (!response.ok) {
                json = await response.json();
            } else {
                json = await response.json();
            }
            return json;
        } catch (error) {
            console.error("ERROR ->");
            console.error(error);
            let valid = { success: false, log: error.toString() + " URL: " + url };
            console.error(valid);
            return valid;
        }
    }
}

export default new GeneralService();
