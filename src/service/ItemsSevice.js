import { GeneralService } from "./GeneralService";

const servicePort = "8081";
const serviceEndPoint = "/items";

class ItemsSevice {
    async queryLstItems(payload) {
        return new GeneralService().asyncPostService(payload, servicePort, serviceEndPoint + "");
    }
}

export default new ItemsSevice();
