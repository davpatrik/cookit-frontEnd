import React, { useState, useEffect, useRef, useContext } from "react";
// Import prime components
import { Button } from "primereact/button";
import { Column } from "primereact/column";
import { ConfirmDialog } from "primereact/confirmdialog";
import { Dialog } from "primereact/dialog";

import { DataTable } from "primereact/datatable";
import { InputText } from "primereact/inputtext";
import { Panel } from "primereact/panel";
import { ListBox } from "primereact/listbox";

// Import Services
import RoutesService from "../service/RoutesService";
// Import Utils
import { label } from "../util/Internationalization";

/*
Store
*/
import { AuthContext } from "../data/AuthContext";

export const ItemsList = (props) => {
    /*
    Variables
    */
    const [lstSelItems, setLstSelItems] = useState([]);
    const [globalFilter, setGlobalFilter] = useState(null);
    const [confirmDlgVisible, setConfirmDlgVisible] = useState(false);
    //const [resultDlgVisible, setResultDlgVisible] = useState(false);
    const [resultRoutes, setResultRoutes] = useState(null);
    const dt = useRef(null);

    /* 
    Init
    */
    useEffect(() => {}, []);

    /*
    Context  
    */
    const context = useContext(AuthContext);

    /*
    Methods
    */
    const selectItem = async (id) => {
        setLstSelItems((oldArray) => [id, ...oldArray]);
    };

    const removeItem = async (id) => {
        let filteredList = lstSelItems.filter((itemX) => itemX !== id);
        setLstSelItems(filteredList);
    };

    const processItems = () => {
        let payload = {
            itemIds: lstSelItems,
        };
        RoutesService.queryRoutesByItems(payload).then((response) => {
            setResultRoutes(response);
        });
        setConfirmDlgVisible(false);
    };

    /*
    Table header & filter
    */
    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">{label[context.selLanguage]["item_selection"]}</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder={label[context.selLanguage]["quickSeaarch"]} />
            </span>
            <Button icon="pi pi-refresh" disabled={lstSelItems.length === 0} className="p-button-secondary" title={label[context.selLanguage]["item_refresh_title"]} onClick={(ev) => setLstSelItems([])} style={{ width: "60px" }} />
            <Button icon="pi pi-check" disabled={lstSelItems.length === 0} label={label[context.selLanguage]["item_process"] + " (" + lstSelItems.length + ")"} title={label[context.selLanguage]["item_process_title"]} style={{ width: "245px" }} onClick={() => setConfirmDlgVisible(true)} />
        </div>
    );

    /* 
    Table templates
    */
    const actionBodyTemplate = (rowData) => {
        let alreadySelected = lstSelItems.filter((itemX) => itemX === rowData.id)[0];
        return (
            <div className="actions">
                {alreadySelected ? (
                    <Button icon="pi pi-times" className="p-button-rounded mr-2 p-button-warning" title={label[context.selLanguage]["item_action_remove"] + " " + rowData.name} onClick={() => removeItem(rowData.id)} />
                ) : (
                    <Button icon="pi pi-check" disabled={alreadySelected} className="p-button-rounded p-button-success mr-2" title={label[context.selLanguage]["item_action_header"] + " " + rowData.name} onClick={() => selectItem(rowData.id)} />
                )}
            </div>
        );
    };

    /*
    Subcomponents
    */
    const lstPicks = !resultRoutes
        ? ""
        : resultRoutes.picks.map((col) => {
              return (
                  <div key={col} className="p-col" style={{ color: "Teal" }}>
                      <div className="p-shadow-3">{col}&nbsp;</div>
                  </div>
              );
          });

    const lstOutOfStock = !resultRoutes
        ? ""
        : resultRoutes.outOfStock.map((col) => {
              return (
                  <div key={col} className="p-col" style={{ color: "IndianRed" }}>
                      <div className="p-shadow-3">{col}&nbsp;</div>
                  </div>
              );
          });

    /**
     * Return
     */
    return (
        <div className="p-grid p-fluid">
            <DataTable
                ref={dt}
                value={context.lstItems}
                dataKey="id, name"
                paginator
                rows={10}
                rowsPerPageOptions={[10, 20]}
                className="datatable-responsive"
                paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                currentPageReportTemplate="Showing {first} to {last} of {totalRecords} products"
                globalFilter={globalFilter}
                emptyMessage="Loding Items.."
                header={header}
            >
                <Column header={label[context.selLanguage]["item_id"]} field="id" sortable></Column>
                <Column header={label[context.selLanguage]["item_name"]} field="name"></Column>
                <Column header={label[context.selLanguage]["item_displayName"]} field="displayName" sortable></Column>
                <Column header={label[context.selLanguage]["item_volume"]} field="volume" sortable></Column>
                <Column header={label[context.selLanguage]["item_station"]} field="station" sortable></Column>
                <Column header={label[context.selLanguage]["item_category"]} field="category" sortable></Column>
                <Column header={label[context.selLanguage]["item_action_header"]} body={actionBodyTemplate}></Column>
            </DataTable>
            <ConfirmDialog
                visible={confirmDlgVisible}
                onHide={() => setConfirmDlgVisible(false)}
                message={label[context.selLanguage]["item_process_confirm_msg"] + lstSelItems + " ?"}
                header="Confirmation"
                icon="pi pi-exclamation-triangle"
                accept={() => processItems()}
                reject={() => setConfirmDlgVisible(false)}
            />
            <Dialog header={label[context.selLanguage]["item_process_result_header"]} visible={resultRoutes !== null} style={{ width: "50vw" }} onHide={() => setResultRoutes(null)} draggable={false}>
                <div className="p-grid">
                    <Panel header={label[context.selLanguage]["item_process_result_picks"]}>{lstPicks}</Panel>
                    &nbsp;
                    <Panel header={label[context.selLanguage]["item_process_result_outOfStock"]}>{lstOutOfStock}</Panel>
                </div>
            </Dialog>
        </div>
    );
};
