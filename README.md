This project was bootstrapped with [Primereact components for React](https://primefaces.org/primereact/showcase/#/).

Below you will find some information about how to download, build and run this project.<br>

## Table of Contents

-   [Pre requirements](#pre-requirements)
-   [Download from GitLab](#dounloading-from-gitLab-repo)
-   [Installing modules](#installing-modules)
-   [Running the project](#running-the-project)
-   [CI/CD](#CI/CD)

## Pre requirements

In order to run successfully this project, install the following software:

[Node](https://nodejs.org/es/download/).

## Downloading from GitLab repo

Please execute the following command in order to download the project from GitLab repo:

```sh
git clone https://gitlab.com/davpatrik/cookit-frontEnd.git
```

## Installing modules

Execute the following command in order to download node modules:

```sh
mpn install
```

## Running the project

After installing modules, run this command to run the project:

```sh
mpn start
```

This runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

I hope you enjoy this Meal kit production app!

## //TODO

CI/CD configuration
Front unit test
